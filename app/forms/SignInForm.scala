import play.api.data.Form
import play.api.data.Forms._
import play.api.libs.json.{ Format, Json}

/**
 * The form which handles the submission of the credentials.
    password: String,
    rememberMe: Boolean
  )

  implicit val jsonFormat: Format[Data] = Json.format[Data]
}