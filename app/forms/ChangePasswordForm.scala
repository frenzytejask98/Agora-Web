package forms

import play.api.data.Forms._
import play.api.data._

/**
 * The `Change Password` form.
 */
object ChangePasswordForm {

  /**
   * A play framework form.
   */
  val form = Form(tuple(
    "current-password" -> nonEmptyText,
    "new-password" -> tuple(
            "main" -> text(minLength=8, maxLength=16),
            "confirm" -> text
        ).verifying(
            // Add an additional constraint: both passwords must match
            "Passwords don't match", password => password._1 == password._2
        )
    )(Account.apply)(Account.unapply)
)
}